package geometry;

public class Triangle {
    private float height;
    private float base;
    public Triangle(float height,float base){
        this.height=height;
        this.base=base;
    }
    public double getArea(float height,float base){
        return height*base/2;
    }
    public String toString(){
        return "The area of this triangle is: "+getArea(this.height,this.base);
    }
}
